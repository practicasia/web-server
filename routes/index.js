var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('index', { title: 'Informe Practica IA' });
});

router.get('/data.json', function (req, res, next) {
    res.sendFile('public/data.json');
});


module.exports = router;
