
function createChart(labelsA, timeA, stepsA) {

    var backgroundColors = [];
    var backgroundColors2 = [];
    var backgroundColors3 = [];
    var hoverBackgroundColors = [];
    var hoverBackgroundColors2 = [];
    var borderColors3 = [];

    labelsA.forEach(function () {
        backgroundColors.push('rgba(15, 151, 151, 1)');
        backgroundColors2.push('rgba(243, 153, 62, 1)');
        backgroundColors3.push('rgba(0, 102, 204, 0.2)');
        hoverBackgroundColors.push('rgba(11, 98, 98, 1)');
        hoverBackgroundColors2.push('rgba(220, 117, 13, 1)');
        borderColors3.push('rgba(0, 102, 204, 1)');
    });

    new Chart(document.getElementById("myChart"), {
        type: 'bar',
        data: {
            labels: labelsA,
            datasets: [{
                label: 'Time',
                data: timeA,
                backgroundColor: backgroundColors,
                borderColor: backgroundColors,
                hoverBackgroundColor: hoverBackgroundColors,
                hoverBorderColor: hoverBackgroundColors,
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        min: 0,
                        max: 36,
                        stepSize: 2,
                        beginAtZero:true
                    }
                }]
            }
        }
    });

    new Chart(document.getElementById("mySecondChart"), {
        type: 'bar',
        data: {
            labels: labelsA,
            datasets: [{
                label: 'Gráfica ampliada para ver los valores entre 0s y 0.025s',
                data: timeA,
                backgroundColor: backgroundColors2,
                borderColor: backgroundColors2,
                hoverBackgroundColor: hoverBackgroundColors2,
                hoverBorderColor: hoverBackgroundColors2,
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        min: 0,
                        max: 0.025,
                        stepSize: 0.001,
                        beginAtZero:true
                    }
                }]
            }
        }
    });

    new Chart(document.getElementById("stepsChart"), {
        type: 'line',
        data: {
            labels: labelsA,
            datasets: [{
                label: 'Número de pasos',
                data: stepsA,
                backgroundColor: backgroundColors3,
                borderColor: borderColors3,
                borderWidth: 3,
                pointRadius: 1
            }]
        },
        options: {
            scales: {
                xAxes: [{
                    stacked: true
                }],
                yAxes: [{
                    ticks: {
                        max: 1700,
                        stepSize: 100,
                        beginAtZero:true
                    }
                }]
            }
        }
    });
}

$.getJSON("/data", function (data) {

    var labels = [];
    var times = [];
    var steps = [];
    var percents = [];

    data.forEach(function (value, index) {
        labels.push(value.row);
        times.push(value.time);
        steps.push(value.steps);
        percents.push(value.percent);
    });

    createChart(labels, times, steps);
});
