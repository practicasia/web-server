# Informe Práctica Inteligencia Artificial
Este servidor web va a servir como informe de la práctica de simulación de coche autónomo para la asignatura de Inteligencia Artificial

## Puesta en marcha
Para el entorno de desarrollo bastará con ejecutar:
```
npm run start
```

Para el entorno de producción:

* Pendiente de crear la tarea en npm para su posterior despliegue
